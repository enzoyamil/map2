import java.io.*;
import java.util.*;
public class Map {
    /*La tasa de Natalidad y mortalidad esta representada como porcentaje*/
    public HashMap<String,Country> addCountry(){
        HashMap<String,Country> map = new HashMap<>();
        try {
            Scanner option = new Scanner(System.in);
            String estado = "1";
            String nameCountry="";
            String continent ="";
            double percetajeBirt =0;
            double percetajeMortality =0;
            int population=0;
            int key = 0;
            System.out.println("Lista de Paises");
            while(estado.equals("1")){
                System.out.println("Ingrese Nombre del Pais");
                nameCountry = option.next();
                System.out.println("Ingrese Continente del País: LatinoAmerica,Europa,Africa,Asia,Oceania,America");
                continent = option.next();
                System.out.println("Ingrese Población");
                population=option.nextInt();
                System.out.println("Ingrese Tasa de Natalidad País(%)");
                percetajeBirt = option.nextDouble();
                System.out.println("Ingrese Tasa de Mortalidad País(%)");
                percetajeMortality = option.nextDouble();
                Country country = new Country(nameCountry,continent,population,percetajeBirt,percetajeMortality);
                map.put(Integer.toString(key),country);
                System.out.println("Desea Agregar mas paises? 1:si 2:no");
                estado= option.next();
                key++;
            }
            writeText(ordenarMapaPorContinentes(map));

            printMap(map);
            File file = new File("C:\\Users\\enzoc\\IdeaProjects\\OrderedMap\\src\\TasaNatalidad.txt");
            readText(file);
            mayorTazaNatalidadContinent(map);
            mayorTazaMortalidadContinent(map);
            return map;
            }catch (Exception e){
            System.out.println("Error Causado por:"+ e.getMessage());
                restart();
            }
        return map;
    }
    public void readText(File file){
        try{
            System.out.println("-------Lectura de Archivo--------");
            FileReader reader = new FileReader(file);
            String text="";
            BufferedReader storage = new BufferedReader(reader);
            while ((text = storage.readLine())!=null){
                System.out.println(text);
            }
            System.out.println("---------------------------------");
            storage.close();
            reader.close();
        }catch (IOException e){
            e.printStackTrace();
        }
    }
    private void writeText(HashMap<String,Country> map){
        try {
            FileWriter writer = new FileWriter("C:\\Users\\enzoc\\IdeaProjects\\OrderedMap\\src\\PaisesIngresados.txt");
            String content="";
            writer.write("|------------------------|");
            writer.write("\r\n");
            writer.write("+ País : ");
            writer.write(" Continente  +");
            writer.write("\r\n");
            writer.write("+------------------------+");
            writer.write("\r\n");
            for (java.util.Map.Entry<String, Country> element : map.entrySet()) {
                writer.write("+ ");
                writer.write(element.getValue().getNameCountry().concat(" : ").concat(element.getValue().getContinent()).concat("+").concat("\r\n"));
            }
            writer.write("+------------------------+");
            writer.close();
        }catch (IOException e){
            e.printStackTrace();
        }
    }

    private void printMap(HashMap<String,Country> map){
        map.entrySet().stream().forEach(element->System.out.println(element.getValue().getNameCountry().concat(": "+ element.getValue().getContinent())));
        System.out.println("-----------------------------");
    }

    private HashMap<String,Country> ordenarMapaPorContinentes(HashMap<String,Country> map){
        System.out.println("--------Mapa Ordenado--------");
       List<java.util.Map.Entry<String,Country>> mapResult = new LinkedList<java.util.Map.Entry<String, Country>>(map.entrySet());
       Collections.sort(mapResult, new Comparator<java.util.Map.Entry<String, Country>>() {
            @Override
            public int compare(java.util.Map.Entry<String, Country> o1, java.util.Map.Entry<String, Country> o2) {
               return o1.getValue().getContinent().compareTo(o2.getValue().getContinent());
            }
       });
        HashMap<String,Country> ordermap = new LinkedHashMap<>();
        for(HashMap.Entry<String,Country> item :mapResult){
            ordermap.put(item.getKey(),item.getValue());
        }
        return ordermap;
   }

   private HashMap<String,Double> mayorTazaNatalidadContinent(HashMap<String,Country> map){
        HashMap<String,Double> mapNatalidad = new HashMap<>();
        double mayorNatalidadLatinoAmerica=0;
        double mayorNatalidadAfrica=0;
        double mayorNatalidadAsia=0;
        double mayorNatalidadEuropa=0;
        double mayorNatalidadOceania=0;
        double mayorNatalidadAmerica=0;
        mayorNatalidadLatinoAmerica = map.entrySet()
                .stream()
                .filter(element->element.getValue().getContinent().equals("LatinoAmerica"))
                .mapToDouble(element->element.getValue().getPercentajebirt()).sum();
        mapNatalidad.put("LatinoAmerica",mayorNatalidadLatinoAmerica);

        mayorNatalidadAfrica = map.entrySet()
                .stream()
                .filter(element->element.getValue().getContinent().equals("Africa"))
                .mapToDouble(element->element.getValue().getPercentajebirt()).sum();
       mapNatalidad.put("Africa",mayorNatalidadAfrica);

       mayorNatalidadAsia = map.entrySet()
               .stream()
               .filter(element->element.getValue().getContinent().equals("Asia"))
               .mapToDouble(element->element.getValue().getPercentajebirt()).sum();
       mapNatalidad.put("Asia",mayorNatalidadAsia);

       mayorNatalidadEuropa = map.entrySet()
               .stream()
               .filter(element->element.getValue().getContinent().equals("Europa"))
               .mapToDouble(element->element.getValue().getPercentajebirt()).sum();
       mapNatalidad.put("Europa",mayorNatalidadEuropa);

       mayorNatalidadOceania = map.entrySet()
               .stream()
               .filter(element->element.getValue().getContinent().equals("Oceania"))
               .mapToDouble(element->element.getValue().getPercentajebirt()).sum();
       mapNatalidad.put("Oceania",mayorNatalidadOceania);

       mayorNatalidadAmerica = map.entrySet()
               .stream()
               .filter(element->element.getValue().getContinent().equals("America"))
               .mapToDouble(element->element.getValue().getPercentajebirt()).sum();
       mapNatalidad.put("America",mayorNatalidadAmerica);
       imprimirTazaNatalidadContinentes(mapNatalidad);
        return mapNatalidad;
   }

    private HashMap<String,Double> mayorTazaMortalidadContinent(HashMap<String,Country> map){
        HashMap<String,Double> mapMortalidad = new HashMap<>();
        double mayorMortalidadLatinoAmerica=0;
        double mayorMortalidadAfrica=0;
        double mayorMortalidadAsia=0;
        double mayorMortalidadEuropa=0;
        double mayorMortalidadOceania=0;
        double mayorMortalidadAmerica=0;
        mayorMortalidadLatinoAmerica = map.entrySet()
                .stream()
                .filter(element->element.getValue().getContinent().equals("LatinoAmerica"))
                .mapToDouble(element->element.getValue().getPercentajeMortality()).sum();
        mapMortalidad.put("LatinoAmerica",mayorMortalidadLatinoAmerica);

        mayorMortalidadAfrica = map.entrySet()
                .stream()
                .filter(element->element.getValue().getContinent().equals("Africa"))
                .mapToDouble(element->element.getValue().getPercentajeMortality()).sum();
        mapMortalidad.put("Africa",mayorMortalidadAfrica);

        mayorMortalidadAsia = map.entrySet()
                .stream()
                .filter(element->element.getValue().getContinent().equals("Asia"))
                .mapToDouble(element->element.getValue().getPercentajeMortality()).sum();
        mapMortalidad.put("Asia",mayorMortalidadAsia);

        mayorMortalidadEuropa = map.entrySet()
                .stream()
                .filter(element->element.getValue().getContinent().equals("Europa"))
                .mapToDouble(element->element.getValue().getPercentajeMortality()).sum();
        mapMortalidad.put("Europa",mayorMortalidadEuropa);

        mayorMortalidadOceania = map.entrySet()
                .stream()
                .filter(element->element.getValue().getContinent().equals("Oceania"))
                .mapToDouble(element->element.getValue().getPercentajeMortality()).sum();
        mapMortalidad.put("Oceania",mayorMortalidadOceania);

        mayorMortalidadAmerica = map.entrySet()
                .stream()
                .filter(element->element.getValue().getContinent().equals("America"))
                .mapToDouble(element->element.getValue().getPercentajeMortality()).sum();
        mapMortalidad.put("America",mayorMortalidadAmerica);
        imprimirTazaMortalidadContinentes(mapMortalidad);
        return mapMortalidad;
    }

   private void imprimirTazaNatalidadContinentes(HashMap<String,Double> map){
       System.out.println("---Taza de Natalidad  Continentes---");
        map.entrySet().stream().forEach(element-> System.out.println(element.getKey()+": "+ element.getValue()));
       System.out.println("------------------------------------");
   }
    private void imprimirTazaMortalidadContinentes(HashMap<String,Double> map){
        System.out.println("---Taza de Mortalidad  Continentes---");
        map.entrySet().stream().forEach(element-> System.out.println(element.getKey()+": "+ element.getValue()));
        System.out.println("------------------------------------");
    }

    private void restart(){
        Scanner option = new Scanner(System.in);
        System.out.println("Ocurrio un Error durante la Ejecución. ¿Desea Salir?");
        System.out.println("1:Si");
        System.out.println("2:No");
        String res = option.next();
        if(res.equals("no")){
            Map m = new Map();
            m.addCountry();
            File file = new File("C:\\Users\\enzoc\\IdeaProjects\\OrderedMap\\src\\TasaNatalidad.txt");
            m.readText(file);
        }else{
            System.exit(0);
        }
    }
}
