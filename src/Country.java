public class Country {
    private String nameCountry;
    private String continent;
    private int population;
    private double percentajebirt;
    private double percentajeMortality;

    public Country(String nameCountry, String continent,int population,double percentajebirt,double percentajeMortality){
        this.nameCountry=nameCountry;
        this.continent=continent;
        this.population=population;
        this.percentajebirt = percentajebirt;
        this.percentajeMortality = percentajeMortality ;
    }

    public String getNameCountry() {
        return nameCountry;
    }

    public void setNameCountry(String nameCountry) {
        this.nameCountry = nameCountry;
    }

    public String getContinent() {
        return continent;
    }

    public void setContinent(String continent) {
        this.continent = continent;
    }

    public int getPopulation() {
        return population;
    }

    public void setPopulation(int population) {
        this.population = population;
    }

    public double getPercentajebirt() {
        return percentajebirt;
    }
    public double getPercentajeMortality() {
        return percentajeMortality;
    }
}
